# jdve-me

This is the source for my website at [jdve.me](https://jdve.me).

## setup

```
asdf install
npm install -g concurrently
```

## dev

```
just serve
```

## build

```
just build
```

## new post

```
just new <post name here>
```

