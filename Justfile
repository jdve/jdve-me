year := datetime('%Y')
month := datetime('%m')

serve:
  concurrently \
    "./themes/congo/node_modules/tailwindcss/lib/cli.js -c \
       ./themes/congo/tailwind.config.js \
       -i ./themes/congo/assets/css/main.css \
       -o ./assets/css/compiled/main.css -w --jit" \
    "hugo server"

build:
  hugo build

new post:
  hugo new content -f content/blog/{{year}}/{{month}}/{{post}}/index.md
  nvim content/blog/{{year}}/{{month}}/{{post}}/index.md

