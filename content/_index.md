---
title: Home
---
Hi! My name is Jonathan Van Eenwyk. I'm a software consultant who enjoys tackling hard problems with simple, creative solutions. Today, I live in India with my family where I'm the CEO of [Vihanti](https://vihanti.com), a software development/consulting company, specializing in mobile apps, web development, and backend cloud software. Working on-site with our development team, I'm able to provide our clients the benefits of software outsourcing without the communication challenges or risks of working across borders.

My development philosophy could perhaps be summarized this way:

> To ask the 'right' question is far more important than to receive the answer.<br>
> — <cite>Jiddu Krishnamurti</cite>

This means never falling into the trap of using a hammer to solve every problem like it's a nail. For me, this philosophy has given me the privilege of using many different programming languages and frameworks, recognizing that each one has its areas of strengths and weaknesses. And that's the fun of it! I love experimenting with the bleeding edge. For many years, this led me towards fringe operating systems (like BeOS). Today, I enjoy playing with less mainstream programming languages (like Haskell and Nim).

