+++
title = 'Connecting Fivetran to YNAB'
date = 2024-11-11T12:53:55+05:30
categories = ['tech', 'business']
+++

Before talking about using [Fivetran](https://fivetran.com) to get [YNAB](https://ynab.com) data into my data warehouse, a personal aside about finances: even before getting married, I was already obsessively frugal with money, tending to save instead of spend. I lived by delayed gratification, often waiting years before buying something until I was sure that it would be worth it. After getting married, while those principles stayed the same, my wife brought rigor and process into how we manage our finances instead of leaving it to feelings. Initially, I was reluctant to make a budget, because it seemed to me that it was simply a way to justify spending more. "Oh, look, we still have $10 left in fun money. Let's spend it!" Gradually, my wife helped me to view budgeting as a tool for saving money rather than a license to spend money.  Together, we've come up with a way of thinking about money that combines my tendency towards frugality and her need for structure. YNAB and especially the [four rules](https://www.ynab.com/the-four-rules) have been instrumental for us.  Now I can comfortably think about a budget as something that provides both structure *and* flexibility. (If you haven't read about the four rules, you should. It's a really helpful concept.)

Today we use YNAB for managing our personal financies in both the US and India.  It's also the foundation for how we manage our two businesses, Vihanti International in the US and Vihanti Digital Services in India. While YNAB is designed for personal budgets, it's actually ideal for [small businesses](https://www.ynab.com/category/small-business) as well. In fact, Jesse Mecham (founder of YNAB) cohosts a podcast with [Mark Butler](https://letsdothebooks.com/services/) (fractional CFO for consulting businesses) called [Beginning Balance](https://www.ynab.com/blog/a-podcast-for-business-owners-by-business-owners), where they frequently talk about how to use YNAB to help manage small business finances.

Anyway, this year I built a data warehouse for the business, using FiveTran to pull the data into [Google Cloud BigQuery](https://cloud.google.com/bigquery).  Although Fivetran doesn't have a connector for YNAB, I was able to built a connector quite easily: [fivetran-ynab](https://gitlab.com/jdve/fivetran-ynab).

![Google Cloud Function](cloud_function.png)

Then, using Fivetran's [Google Cloud Functions connector](https://fivetran.com/docs/connectors/functions/google-cloud-functions/setup-guide), I configured the connector to use my own Google Cloud Function to grab the data from YNAB. My connector just needs the YNAB api key and budget id to be provided in the connector configuration. That's it!

![Fivetran Connector](fivetran.png)

Now I have all of my YNAB data easily available alongside the business' CRM data ([Airtable](https://airtable.com)), timesheet data ([Everhour](https://everhour.com)), and project management data ([Linear](https://linear.com)). Win!

