+++
title = 'Organising notes in FSNotes using the Zettelkasten method'
date = 2024-08-20
categories = ["productivity"]
+++

Over the last decade, I've increasingly adopted the [Zettelkasten](https://zettelkasten.de/overview/) note-taking system. I've also become a strong proponent for non-proprietary, long-lasting formats like [Markdown](https://www.markdownguide.org) to avoid vendor lock-in. As a result, I've gradually migrated nearly all of my documents into Markdown format. Since most of my documents are notes or thoughts spread over time, I've also found it helpful to name the files using a [timestamp-based ID](https://zettelkasten.de/posts/add-identity/).  Combined with a good note taking tool, my documents are conveniently organised date-first, but also easily searchable. I keep my notes organised into a few top-level folders. I use a few dozen tags to further categorise documents. I prefer to limit the number of tags. I try to follow the [atomic principle](https://zettelkasten.de/posts/create-zettel-from-reading-notes/) of limiting each note to one main thought. I use links as needed, but not nearly as much as full-time researchers, perhaps because my notes tend to be less strongly related to each other (aside from the tags).

For the last several years, [FSNotes](https://fsnot.es) is my note editor of choice.

![FSNotes folder organisation](fsnotes1.png)

I appreciate it's clean, simple design. My folders and tags appear on the left side. My notes appear on the right side in minimally-formatted Markdown.

![FSNotes notes display](fsnotes2.png)

From a tools perspective, one of the most important things that I've learned about note taking is the importance of reducing note-taking friction. It's gotta be easy, otherwise I won't do it. To that end, FSNotes is one of the few apps that I leave running all the time on my laptop, and I simply hit `⌘+N` to start a new note. I really appreciate that FSNotes supports automatically naming new files using the current time.

![FSNotes automatic file naming](fsnotes3.png)

