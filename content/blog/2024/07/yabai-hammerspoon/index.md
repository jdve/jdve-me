+++
title = 'Using yabai with hammerspoon'
date = 2024-07-17T09:42:37+05:30
categories = ["tech", "productivity"]
+++

Back when I used Ubuntu as my daily work machine, I fell in love with the [xmonad](https://xmonad.org) window manager. It's an amazing feeling to not have to mess around with window placements and just let the computer automatically tile the windows according to sensible rules. Later, when I switched to MacOS, I looked for the same experience. After trying a number of other tools, I finally landed on [yabai](https://github.com/koekeishiya/yabai), and I've been using it for many years now. The same author has built a global keyboard shortcut manager called [skhd](https://github.com/koekeishiya/skhd). It works well in combination with yabai.

However, a few months ago, I realised that since I was already using [Hammerspoon](https://www.hammerspoon.org) for other global keyboard shortcuts, I might as well use Hammerspoon to manage yabai. This combination works great with a few nifty features that aren't available with skhd (which is strictly a shortcut manager):

1. Using Hammerspoon's [ModalMgr](https://www.hammerspoon.org/Spoons/ModalMgr.html), I can show a small green circle in the bottom corner of the screen to indicate that I'm in "yabai" mode. Slick!

![yabai mode indicator circle](mode.png)

2. While in "yabai" mode, hitting "tab" shows a cheatsheet with all the available shortcuts. It's a nice way to remind myself when I forget a less frequently used shortcut.

![yabai cheatsheet](cheatsheet.png)

My [yabai.lua](https://gitlab.com/jdve/dotfiles/-/blob/main/macos/.hammerspoon/yabai.lua) Hammerspoon configs are part of my public dotfiles.

