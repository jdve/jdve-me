+++
title = 'My Life in Weeks'
date = 2025-02-18T15:14:14+05:30
categories = ['Life']
+++

I recently saw [Gina Trapani](https://ginatrapani.org)'s [Life in Weeks](https://weeks.ginatrapani.org/) on [Hacker News](https://news.ycombinator.com/item?id=43061498). It's a really neat way to visualize one's entire life. So I thought I'd [adapt it for myself](https://gitlab.com/jdve/jdve-me/-/blob/main/layouts/partials/life-in-weeks/index.html). In my version, all the weeks are equally sized so that one year fits on a single line. The colors represent where I was living at the time. After I got married, the squares are outlined. Hover over them to see more details. I'm pretty happy with how it turned out!

{{< life-in-weeks start_date="1983-03-07" end_year="2073" >}}

