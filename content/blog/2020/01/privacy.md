+++
categories = ["web", "business"]
date = "2020-01-29"
summary = "A better way to privacy policy bliss"
title = "Privacy Policies and Terms of Service"
+++

If you've ever tried to add a privacy policy or terms of service to your website, you know the pain.  Just try searching for "privacy policy template" in Google, and you'll be overwhelmed with "free generators" that are only sort of free.  Not only do they intentionally leave out the important parts of the policy unless you pay, many of them also seem quite poorly written.

Thankfully, [Basecamp](https://basecamp.com/) has generously [open sourced](https://github.com/basecamp/policies) their privacy policy and terms of service!  Plus, their version is actually readable by normal humans, unlike so many that are filled with legal jargon.

