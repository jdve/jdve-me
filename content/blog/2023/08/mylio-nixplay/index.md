+++
date = "2023-08-28"
summary = "How to automatically send selected photos from Mylio to Nixplay"
title = "Sending Mylio photos to Nixplay"
categories = ["tech"]
+++

This post shows how I automated sharing photos from [Mylio](http://mylio.com) to my family's [Nixplay](https://www.nixplay.com) photo frames.

1. Add a Google Drive as a cloud service in Mylio.
2. Turn off syncing originals and optimised images *except* for originals that match the blue flag. This means that only originals flagged with blue will be uploaded to Google Drive. Mylio will still upload thumbnails, but those are small so it doesn't matter that much.
3. Create a new scenario in make.com. This scenario will watch for new files in Google Drive, make sure they are originals (not thumbnails), download the image, and then send an image to Nixplay with the photo as an attachment. Here's what the scenario looks like:

   ![Make scenario](make-scenario.jpg)

   The first "Is Image" filter simply looks for images that start with `n_` (which are originals):

   ![Filtering images](make-images.jpg)

   The second "Is Mylio" filter checks for files that are in the `/Mylio` directory:

   ![Filtering path](make-path.jpg)

That's it! As soon as I tag an image in Mylio with the blue flag, Mylio will immediately upload it to Google Drive. Then, make.com will send out an email with the photo as an attachment. So much easier than manually exporting photos and sending emails!

